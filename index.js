//EXPRESS SETUP
// ~import expres by using the 'require' directive
const express = require('express')
// ~use the express() function and assign it to the app variable
const app = express()
// ~declare a variable for the port of this server
const port = 3000

// ~use these middleware to enable our server to read JSON as regular JS
app.use(express.json())
app.use(express.urlencoded({extended: true}))

// ~ you can have your routes after all of that.
//ROUTES
//Landing page route
app.get('/', (req, res) => {
	res.send(`Hello World!`)
})

//Post request routee
app.post('/hello', (req, res) => {
	res.send(`Hello there ${req.body.firstName} ${req.body.lastName}!`)
})

//Register user route
let users = []

app.post('/register', (request, response) => {
	console.log(request.body)

	if(request.body.username !== '' && request.body.password !== '') {
		users.push(request.body)
		console.log(users)
		response.send(`User ${request.body.username} successfully registered.`)
	} else {
		response.send(`Please input BOTH username and password.`)
	}
})

//kebab casing
app.put('/change-password',(req, res) => {
	let message
	// loop through the whole users array (declared abov) so that the server can check if the username that was placed from the request matches any of the existing usernames inside that array
	for(let i= 0; i < users.length; i++){
		// if a username matches, reassign/change the existing user's password
		if(req.body.username == users[i].username){
			users[i].password = req.body.password

			message = `User ${req.body.username}'s password has been updated!`

			break
		} else { //if no username matches, set the message variable to a string saying that the user does not exist
			message = 'User does not exist.'
		}
	}
	// send a response with the respective message depending on the result of the condition
	res.send(message)
})


app.listen(port, () => console.log(`Server is running at port ${port}`))



